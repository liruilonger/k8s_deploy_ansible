#!/bin/bash

#@File    :   update_kernel
#@Time    :   2023/02/01 23:58:23
#@Author  :   Li Ruilong
#@Version :   1.0
#@Desc    :   contos 7 批量升级内核脚本
#@Contact :   liruilonger@gmail.com



yum -y install https://www.elrepo.org/elrepo-release-7.el7.elrepo.noarch.rpm 

rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org

yum -y  --enablerepo=elrepo-kernel install kernel-lt.x86_64

grub2-set-default 0

grub2-mkconfig -o /boot/grub2/grub.cfg

reboot
