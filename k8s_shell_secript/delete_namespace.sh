#!/bin/bash

coproc kubectl proxy --port=30990 &

if [ $# -eq 0 ] ; then
	echo "后面加上你所要删除的ns."
	exit 1
fi

kubectl get namespace $1 -o json > logging.json
sed  -i '/"finalizers"/{n;d}' logging.json
curl -k -H "Content-Type: application/json" -X PUT --data-binary @logging.json http://127.0.0.1:30990/api/v1/namespaces/${1}/finalize

kill %1
