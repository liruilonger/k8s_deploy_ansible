ETCDCTL_API=3 etcdctl snapshot restore ./snap-202301272133.db \
--name vms81.liruilongs.github.io  \
--cert="/etc/kubernetes/pki/etcd/server.crt" \
--key="/etc/kubernetes/pki/etcd/server.key"  \
--cacert="/etc/kubernetes/pki/etcd/ca.crt"   \ 
--endpoints="https://127.0.0.1:2379" \
--initial-advertise-peer-urls="https://192.168.26.100:2380"  \ 
--initial-cluster="vms100.liruilongs.github.io=https://192.168.26.100:2380" \
--data-dir=/var/lib/etcd
