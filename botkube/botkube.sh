export CLUSTER_NAME=kubernetes
export ALLOW_KUBECTL=true
export ALLOW_HELM=true
export SLACK_CHANNEL_NAME=alert
export SLACK_API_APP_TOKEN=xapp-1-A04MMJ6UH8V-4724060875543-74c8e238209a99d6990346a51bdbefac7766d24ecd9e3dcf02aadc2b085ba000
export SLACK_API_BOT_TOKEN=xoxb-4582566940866-4731960211174-dHLSSf1wJvGmncQh0If3YXtm


helm install --version v0.17.0 botkube --namespace botkube --create-namespace \
--set communications.default-group.socketSlack.enabled=true \
--set communications.default-group.socketSlack.channels.default.name=${SLACK_CHANNEL_NAME} \
--set communications.default-group.socketSlack.appToken=${SLACK_API_APP_TOKEN} \
--set communications.default-group.socketSlack.botToken=${SLACK_API_BOT_TOKEN} \
--set settings.clusterName=${CLUSTER_NAME} \
--set executors.kubectl-read-only.kubectl.enabled=${ALLOW_KUBECTL} \
--set 'executors.helm.botkube/helm.enabled'=${ALLOW_HELM} \
botkube/botkube

#SLACK_CHANNEL_NAME是添加@Botkube 的频道名称
#SLACK_API_BOT_TOKEN是您在将 Botkube 应用程序安装到您的 Slack 工作区后收到的令牌
#SLACK_API_APP_TOKEN是您在将 Botkube 应用程序安装到 Slack 工作区后收到的令牌，并在 App-Level Token 部分生成
#CLUSTER_NAME是传入消息中设置的集群名称
#ALLOW_KUBECTL设置为 true 以允许kubectlBotkube 在集群上执行命令，
#ALLOW_HELM设置为 true 以允许helmBotkube 在集群上执行命令，
